import logging
import time
import snap7

from homeassistant.const import TEMP_CELSIUS
from homeassistant.helpers.entity import Entity

DOMAIN = "Siemens_logo*"

#from homeassistant.const import CONF_HOST, CONF_REMOTE_TSAP, CONF_LOCAL_TSAP

_LOGGER = logging.getLogger('homeassistant.components.siemens_logo)


DEFAULT_NAME = 'Sonoff Switch'
DEFAULT_ICON = 'mdi:flash'


async def async_setup_platform(hass, config, async_add_entities,
                               discovery_info=None):

#    host = config[CONF_HOST]
#    remote_tsap = config[CONF_REMOTE_TSAP]
#    local_tsap = config[CONF_LOCAL_TSAP]

    async_add_entities([ ExampleSensor()], True)


class ExampleSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self):
        """Initialize the sensor."""
        self._state = None

    @property
    def name(self):
        """Return the name of the sensor."""
        return 'Example Temperature'

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return TEMP_CELSIUS

    def update(self):
        """Fetch new state data for the sensor.
        This is the only method that should fetch new data for Home Assistant.
        """
        plc = snap7.logo.Logo()
        plc.connect("192.168.32.12", 0x1000,0x2000)
        time.sleep(0.1)
        self._state = plc.read("VW0")/100
        plc.disconnect()


