import logging
import time
import snap7

from homeassistant.const import TEMP_CELSIUS
from homeassistant.helpers.entity import Entity

DOMAIN = "Siemens_logo*"

from homeassistant.const import CONF_HOST

#_LOGGER = logging.getLogger('homeassistant.components.siemens_logo)


DEFAULT_NAME = 'Sonoff Switch'
DEFAULT_ICON = 'mdi:flash'

#CONF_REMOTE_TSAP, CONF_LOCAL_TSAP, CONF_VM_ADDRESS, CONF_MULTIPLIER

async def async_setup_platform(hass, config, async_add_entities,
                               discovery_info=None):

    host = config[CONF_HOST]
#    remote_tsap = config[CONF_REMOTE_TSAP]
#    local_tsap = config[CONF_LOCAL_TSAP]
    address = config["address"]
    multiplier = float(config["multiplier"])
    name = config["name"]

    async_add_entities([ ExampleSensor(host, address, multiplier, name)], True)


class ExampleSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self, host, address, multiplier, name):
        """Initialize the sensor."""
        self._host = host
        self._address = address
        self._multiplier = multiplier
        self._name = name

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return TEMP_CELSIUS

    def update(self):
        """Fetch new state data for the sensor.
        This is the only method that should fetch new data for Home Assistant.
        """
        plc = snap7.logo.Logo()
        plc.connect(self._host, 0x1000,0x2000)
        time.sleep(0.1)
        self._state = plc.read(self._address) * self._multiplier
        plc.disconnect()

