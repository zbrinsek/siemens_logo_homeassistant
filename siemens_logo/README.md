libsnap7.so is tested with 109.6

copy libsnap7.so to /usr/lib/

in ha console 
```bash
    login
    docker container ls
    docker exec -it 3f3 /bin/bash
    cp /config/custom_components/siemens_logo/libsnap7.so /usr/lib/
```   

    
for compile download latest from https://sourceforge.net/projects/snap7/files/

in docker
```bash
    apk add make
    apk add g++
    cd snap7-full*/build/unix
    make -f x86_64_linux.mk all
    make -f x86_64_linux.mk install
```

congiguration.yaml
```
sensor:
  - platform: siemens_logo
    host: 192.168.0.6
    address: VW0
    multiplier: 0.01
    name: temp
    entity_id: sensor_0
    
  - platform: siemens_logo
    host: 192.168.0.7
    address: VW2
    multiplier: 0.1
    name: humidity
    entity_id: sensor_1   
  
switch:
  - platform: siemens_logo
    host: 192.168.0.6
    address: V20.1
    name: up
    icon: 'mdi:pan-up'

```
